const int motor = 10; // control to pump
const int ledon = 7;  // LED when the motor is running
const int ledoff = 8; // LED when the motor is off

void setup() {
  pinMode(motor, OUTPUT);
  pinMode(ledon, OUTPUT);
  pinMode(ledoff, OUTPUT);
}

void loop() {
  digitalWrite(ledon,HIGH);
  digitalWrite(ledoff,LOW);
  digitalWrite(motor,HIGH);
  delay(5000);
  digitalWrite(ledon,LOW);
  digitalWrite(ledoff,HIGH);
  digitalWrite(motor,LOW);
  delay(5000);
}