# arduino-peristaltic-pump

Code to run a 12V DC peristaltic pump

# Parts
- 1 Arduino Uno R3
- 1 breadboard
- 1 12V DC peristaltic pump
- 1 IN4007 diode
- 1 PN2222 transistor
- 2 LEDs (I used green and red)
- 2 10K resistors
- various wires

# Setup
It's important to note at 5V, the pump will run slower. Other guides recommend using a higher voltage battery for higher flow.  

I'm also not an electrical engineer, so please bear with me..

## Pump
- Run the 5V to a row in the breadboard. In the same row:
  - add diode -
  - add wire to the pump +
- Run wire from pump - to another row
  - add diode +
  - transistor B
- Run wire from Arduino 10 to transistor C
- Run wire from transistor E to GND

## LEDs
- 10K resistor from Arduino 7 to LED + (green)
- 10K resistor from Arduino 8 to LED + (red)
- Run wire from both LED - to GND

# Reference
https://rcciit.org/students_projects/projects/aeie/2018/GR8.pdf
https://www.instructables.com/id/Automatically-water-your-small-indoor-plant-using-/
http://www.learningaboutelectronics.com/Articles/Peristaltic-pump-circuit-with-an-arduino-microcontroller.php